using UnityEngine;
using UnityEngine.UI;

public class ControlMainMenu : MonoBehaviour
{
    public Slider soundSlider;
    public Slider musicSlider;
    
    private float soundVolume;
    private float musicVolume;
    
    private void Start()
    {
        if (soundSlider != null)
        {
            soundVolume = GameSettings.LoadSound();
            musicVolume = GameSettings.LoadMusic();

            soundSlider.value = soundVolume;
            musicSlider.value = musicVolume;
            
            ChangeVolumeSound(soundVolume);
            ChangeVolumeMusic(musicVolume);
        }
    }
    public void ChangeVolumeSound(float newVolume)
    {
        soundVolume = newVolume;
        float correctVolumeSound = Mathf.Log10(newVolume) * 20;
        GameSettings.Instance.audioMixer.SetFloat("soundV", correctVolumeSound);
    }
    
    public void ChangeVolumeMusic(float newVolume)
    {
        musicVolume = newVolume;
        float correctVolumeMusic = Mathf.Log10(newVolume) * 20;
        GameSettings.Instance.audioMixer.SetFloat("musicV", correctVolumeMusic);
    }
    
    public void ExitSaveVolume()
    {
        GameSettings.SaveSound(soundVolume);
        GameSettings.SaveMusic(musicVolume);
    }
}
