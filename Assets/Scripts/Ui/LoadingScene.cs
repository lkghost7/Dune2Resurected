﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingScene : GenericSingletonClass<LoadingScene>
{
    [SerializeField][Tooltip("шторка")] private GameObject sceneLoading = null;
    // [SerializeField][Tooltip("подсказки")] private Text tripDescription = null;
    
    private SceneIndex loadingScene;
    private AsyncOperation loadOperation;

    public override void Awake()
    {
        base.Awake();
        sceneLoading.SetActive(false);
        SceneManager.LoadSceneAsync((int) SceneIndex.MAIN_MENU, LoadSceneMode.Additive);
        loadingScene = SceneIndex.MAIN_MENU;
    }

    public void LoadGame() // загрзка первой сцены
    {
        LoadScene(SceneIndex.LEVEL_ONE, true);
    }

    public void LoadScene(SceneIndex sceneIndex, bool showCurtains = true)
    {
        if (showCurtains)
        {
            ShowСurtain();
        }

        loadingScene = sceneIndex;
        UnloadScenes();
    }

    private void UnloadScenes()
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex((int) SceneIndex.LOADING_SCENE));
        if (SceneManager.GetSceneByBuildIndex((int) SceneIndex.MAIN_MENU).isLoaded)
        {
            StartCoroutine(UnloadSceneOperation(SceneIndex.MAIN_MENU));
        }
        else if (SceneManager.GetSceneByBuildIndex((int) SceneIndex.LEVEL_ONE).isLoaded)
        {
            StartCoroutine(UnloadSceneOperation(SceneIndex.LEVEL_ONE));
        }
        else if (SceneManager.GetSceneByBuildIndex((int) SceneIndex.INTRO_PARALLAX).isLoaded)
        {
            StartCoroutine(UnloadSceneOperation(SceneIndex.INTRO_PARALLAX));
        }
        else if (SceneManager.GetSceneByBuildIndex((int) SceneIndex.FINAL_CREDITS).isLoaded)
        {
            StartCoroutine(UnloadSceneOperation(SceneIndex.FINAL_CREDITS));
        }
        else
        {
            StartCoroutine(LoadSceneOperation(loadingScene));
        }
    }

    IEnumerator LoadSceneOperation(SceneIndex sceneIndex)
    {
        loadOperation = SceneManager.LoadSceneAsync((int) sceneIndex, LoadSceneMode.Additive);

        while (!loadOperation.isDone)
        {
            yield return new WaitForSeconds(1f);
        }

        if (sceneIndex == SceneIndex.LEVEL_ONE)
        {
            print("сцена загужена");
        }
        else
        {
            print("игра загружается");
        }
    }

    IEnumerator UnloadSceneOperation(SceneIndex sceneIndex)
    {
        AsyncOperation operation = SceneManager.UnloadSceneAsync((int) sceneIndex);

        while (!operation.isDone)
        {
            yield return null;
        }

        StartCoroutine(LoadSceneOperation(loadingScene));
    }

    public void ShowСurtain()
    {
        sceneLoading.SetActive(true);
    }

    public void HideCurtain()
    {
        sceneLoading.SetActive(false);
    }
}