
using UnityEngine;

public class MoveTest : MonoBehaviour
{
    [Tooltip("обьект котороый перемещается")]public GameObject movableGameObject;
    [Tooltip("позиция куда поедет")]public Transform target;
    [Tooltip("скорость")]public float speed =3;
    [Tooltip("включить движение")]public bool on = true;
    

    void Update()
    {
        if (!on)
        {
            return;
        }
        movableGameObject.transform.position = Vector3.MoveTowards(movableGameObject.transform.position, target.position, speed * Time.deltaTime);
        movableGameObject.transform.LookAt(target);
    }
}
