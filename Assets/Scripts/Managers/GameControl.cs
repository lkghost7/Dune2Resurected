using System.Collections;
using UnityEngine;

public class GameControl : MonoBehaviour
{

    [SerializeField][Tooltip("задержка показа шторки")] private float delayCurtain = 3;
    
    private void Start()
    {
        StartCoroutine(InitGame());
    }

    IEnumerator InitGame()
    {
        yield return new WaitForSecondsRealtime(delayCurtain);
        LoadingScene.Instance.HideCurtain(); // закрыть шторку
    }
}
