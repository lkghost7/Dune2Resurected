﻿using UnityEngine;
using UnityEngine.Audio;

public class GameSettings : GenericSingletonClass<GameSettings>
{
    private const string KEY_SOUND_VOLUME = "settings.sound.volume";
    private const string KEY_MUSIC_VOLUME = "settings.music.volume";
    
    [Header("audio settings")]
    public AudioMixer audioMixer;

    public static void SaveSound(float volumeSave)
    {
        PlayerPrefs.SetFloat(KEY_SOUND_VOLUME, volumeSave);
    }
    
    public static void SaveMusic(float volumeSave)
    {
        PlayerPrefs.SetFloat(KEY_MUSIC_VOLUME, volumeSave);
    }
    
    public static float LoadSound()
    {
        return PlayerPrefs.GetFloat(KEY_SOUND_VOLUME, 1f);
    }
    
    public static float LoadMusic()
    {
        return PlayerPrefs.GetFloat(KEY_MUSIC_VOLUME, 1f);
    }

    private void Start()
    {
        LoadMusic();
        LoadSound();
    }
    
    // public void ChangeVolumeSound(float newVolume)
    // {
    //     soundVolume = newVolume;
    //     float correctVolumeSound = Mathf.Log10(newVolume) * 20;
    //     GameSettings.Instance.audioMixer.SetFloat("soundV", correctVolumeSound);
    // }
    //
    // public void ChangeVolumeMusic(float newVolume)
    // {
    //     musicVolume = newVolume;
    //     float correctVolumeMusic = Mathf.Log10(newVolume) * 20;
    //     GameSettings.Instance.audioMixer.SetFloat("musicV", correctVolumeMusic);
    // }
    //
    // public void ExitSaveVolume()
    // {
    //     GameSettings.SaveSound(soundVolume);
    //     GameSettings.SaveMusic(musicVolume);
    // }
}
