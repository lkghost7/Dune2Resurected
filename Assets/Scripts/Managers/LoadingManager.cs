using UnityEngine;

public class LoadingManager : MonoBehaviour
{
    public void LoadingLevelOne()
    {
        LoadingScene.Instance.LoadGame(); // запуск по кнопке
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
