﻿public enum SceneIndex
{
    LOADING_SCENE = 0,
    MAIN_MENU = 1,
    LEVEL_ONE = 2,
    FINAL_CREDITS = 88,
    INTRO_PARALLAX = 70
}